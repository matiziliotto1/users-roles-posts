<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rolUser = Role::where('name', 'user')->first();

        if (!$rolUser) {
            $rolUser = new Role();
            $rolUser->name = 'user';
            $rolUser->display_name = 'Usuario';
            $rolUser->save();
        }

        $rolAdmin = Role::where('name', 'admin')->first();

        if (!$rolAdmin) {
            $rolAdmin = new Role();
            $rolAdmin->name = 'admin';
            $rolAdmin->display_name = 'Administrador';
            $rolAdmin->save();
        }
    }
}
