<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role_x_user extends Model
{
    use HasFactory;

    public $table = 'roles_x_users';
}
