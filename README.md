# Users & Roles

## Instalación

- Clonar repositorio
- Instalar dependencias de PHP con ```composer install```
- Instalar dependencias de Node con ```npm install```
- Compilar los assets con ```npm run dev```
- Crear una base de datos MySQL local
- Crear archivo ```.env``` en el root, basado en el ```.env.example```, y agregar los datos de configuración necesarios
- Migraciones de base de datos con ```php artisan migrate```
- Crear los valores por defecto en las tablas ```php artisan db:seed```

### Comandos de GIT para subir un proyecto (cuando lo creas desde 0 en local)
-git init
(
-git remote add origin git@gitlab.com:matiziliotto1/users-roles-posts.git
-git remote set-url origin https://gitlab.com/matiziliotto1/users-roles-posts
)
Se usa cualquiera de las lineas anteriores, reemplazando el url del repositorio remoto, por el que corresponda
-git add .
-git commit -m "First commit"
-git push origin master

### Comandos de GIT para bajar un proyecto
git clone https://gitlab.com/matiziliotto1/users-roles-posts.git

### Comandos mas utilizados cuando ya esta subido/bajado el proyecto
-git add .
-git commit -m "First commit"
-git push origin master


### Comandos utiles y necesarios para crear archivos en el entorno Laravel

#### Crear modelos y migraciones
-php artisan make:model Role -m

#### Crear seeders
php artisan make:seeder RoleSeeder

#### Crear controladores
- Ejecutar ```php artisan make:contoller Class ```
- Ejecutar ```php artisan make:contoller Class --resource ```